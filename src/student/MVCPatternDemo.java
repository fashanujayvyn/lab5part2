/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package student;

/**
 *
 * @author jayvy
 */
public class MVCPatternDemo {
    public static void main(String [] args){
        
        Student model = retriveStudentFromDatabase();
        
        
        StudentView view = new StudentView();
        StudentController controller = new StudentController (model, view);
        
        controller.updateView();
        
        controller.setStudentName("John");
controller.updateView();
       
        
    }
    private static Student retriveStudentFromDatabase(){
        Student student = new Student();
        student.setName("Robert");
        student.setRollNo("10");
        student.setId("6529293");
        return student;
    }
}
